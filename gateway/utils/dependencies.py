from fastapi import Security
from fastapi.security import APIKeyHeader

import utils.auth


async def decode_access_token(authorization: str = Security(APIKeyHeader(name="authorization"))):
    return utils.auth.decode_access_token(authorization=authorization)
