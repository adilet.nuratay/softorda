import aiohttp
import async_timeout

from config import settings


async def make_request(
        url: str,
        method: str = 'get',
        data: dict = None,
        json: dict = None,
        headers: dict = None
):
    with async_timeout.timeout(settings.GATEWAY_TIMEOUT):
        async with aiohttp.ClientSession() as session:
            request = getattr(session, method)
            if data is not None:
                async with request(url=url, data=data, headers=headers, ssl=False) as response:
                    data = await response.json()
                    return data, response.status

            async with request(url=url, json=json, headers=headers, ssl=False) as response:
                data = await response.json()
                return data, response.status
