import datetime

import jwt
from fastapi import Security, HTTPException
from fastapi.security import APIKeyHeader
import aiohttp
import async_timeout
from dateutil import parser

from utils.exceptions import AuthTokenMissing, AuthTokenExpired, AuthTokenCorrupted
from config import settings


def decode_access_token(authorization: str = Security(APIKeyHeader(name="authorization"))):
    if not authorization:
        raise AuthTokenMissing('Auth token is missing in headers.')

    token = authorization.replace('Bearer ', '')

    try:
        payload = jwt.decode(jwt=token,
                             key=settings.JWT_SECRET_KEY,
                             algorithms=[settings.JWT_ALGORITHM],
                             options={"verify_signature": False})
        return payload.get("user_name")
    except jwt.exceptions.ExpiredSignatureError:
        raise AuthTokenExpired('Auth token is expired.')
    except jwt.exceptions.DecodeError:
        raise AuthTokenCorrupted('Auth token is corrupted.')


async def check_access(username: str, resource_id: int, amount: int):
    with async_timeout.timeout(delay=settings.GATEWAY_TIMEOUT):
        async with aiohttp.ClientSession() as session:
            async with session.get(url=f"https://kompra.kz/access-limits/limits/get/limit?username={username}&access_limit_id={resource_id}") as r:
                data = await r.json()
                if data is not None:
                    if data['period_type_id'] == 6:
                        return True
                    if parser.parse(data.get("date_from")) <= datetime.datetime.now() <= parser.parse(data.get("date_to"))\
                            and amount <= data.get("left_count"):
                        return True

    raise HTTPException(status_code=403, detail={"status": False, "message": "no-access"})


async def decrement_limits(username: str, resource_id: int, amount: int):
    with async_timeout.timeout(delay=settings.GATEWAY_TIMEOUT):
        async with aiohttp.ClientSession() as session:
            async with session.put(url=f"https://kompra.kz/access-limits/limits/update/limit?username={username}&access_limit_id={resource_id}&amount={amount}") as r:
                data = await r.json()
                if data is not None:
                    if parser.parse(data.get("date_from")) < datetime.datetime.utcnow() < parser.parse(data.get("date_to")) and amount <= data.get("left_count"):
                        return True

    raise HTTPException(status_code=400, detail={"status": False, "message": "access-decrement-error"})
