from typing import Optional
from pydantic import BaseModel, root_validator

from fastapi.exceptions import HTTPException


class SearchBody(BaseModel):
    name: Optional[str] = ""
    inn: Optional[str] = ""
    pinfl: Optional[str] = ""

    @root_validator
    def validate_all_fields(cls, values):
        if not any([values.get('name'), values.get('inn'), values.get('pinfl')]):
            raise HTTPException(status_code=400, detail="Empty search criteria!")
        return values


class SearchUnfairBody(BaseModel):
    company_name: Optional[str] = ''
    resident_inn: Optional[str] = ''
    resident_name: Optional[str] = ''

    @root_validator
    def validate_all_fields(cls, values):
        if not any([values.get('company_name'), values.get('resident_inn'), values.get('resident_name')]):
            raise HTTPException(status_code=400, detail="Empty search criteria!")
        return values


class SearchWantedBody(BaseModel):
    name: Optional[str] = ""
    type: Optional[str] = ""

    @root_validator
    def validate_all_fields(cls, values):
        if not any([values.get('name'), values.get('type')]):
            raise HTTPException(status_code=400, detail="Empty search criteria!")
        return values
