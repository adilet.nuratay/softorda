import os

from dotenv import load_dotenv, find_dotenv

from configs.uz_check_configs import UZSearchConfig


load_dotenv(dotenv_path=find_dotenv())


class Settings(
    UZSearchConfig
):
    PROJECT_NAME: str = os.getenv(key="PROJECT_NAME")
    PROJECT_VERSION: str = os.getenv(key="PROJECT_VERSION")
    PROJECT_DESCRIPTION: str = os.getenv(key="PROJECT_DESCRIPTION")
    ROOT_PATH: str = os.getenv("ROOT_PATH", "")

    APP_ENV: str = os.getenv(key="APP_ENV", default="production")
    APP_DEBUG: bool = os.getenv(key="APP_DEBUG", default=False)
    APP_LOG_LEVEL: str = os.getenv(key="APP_LOG_LEVEL", default="info")

    GATEWAY_TIMEOUT: float = 60.0


settings = Settings()
