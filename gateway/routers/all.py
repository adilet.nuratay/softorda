from fastapi import APIRouter

from routers.uz_check import router as uz_router

router = APIRouter()


router.include_router(router=uz_router, prefix="/uz_check", tags=["UZ Check routes"])
