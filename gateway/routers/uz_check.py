from fastapi import APIRouter, Depends, Request
from fastapi.responses import JSONResponse

from utils.dependencies import decode_access_token

from schemas.schemas import SearchBody, SearchUnfairBody, SearchWantedBody
from utils.network import make_request
from utils import auth
from config import settings


router = APIRouter()


@router.post(path="/search")
async def get_search(request: Request,
                     search_body: SearchBody):
    payload = {
        "name": search_body.name,
        "tin": search_body.inn,
        "pinfl": search_body.pinfl,
    }
    res, status_code = await make_request(url=settings.UZ_SEARCH_URL, method=request.scope.get("method").lower(), json=payload)
    return JSONResponse(status_code=status_code, content=res, media_type="application/json;charset=utf-8")


@router.post(path="/details")
async def get_details(request: Request,
                      search_body: SearchBody,
                      # username: str = Depends(dependency=decode_access_token)
                      ):
    # await auth.check_access(username=username, resource_id=settings.UZ_DETAILS_ID, amount=1)
    payload = {
        "name": search_body.name,
        "inn": search_body.inn,
        "pinfl": search_body.pinfl,
    }
    res, status_code = await make_request(url=settings.UZ_DETAILS_URL,
                                          method=request.scope.get("method").lower(),
                                          json=payload)
    return JSONResponse(status_code=status_code, content=res, media_type="application/json;charset=utf-8")


@router.post(path="/nds_payer")
async def get_details(request: Request,
                      inn: str,
                      # username: str = Depends(dependency=decode_access_token)
                      ):
    # await auth.check_access(username=username, resource_id=settings.UZ_DETAILS_ID, amount=1)
    res, status_code = await make_request(url=settings.UZ_NDS_PAYER_URL.format(inn),
                                          method=request.scope.get("method").lower())
    return JSONResponse(status_code=status_code, content=res, media_type="application/json;charset=utf-8")


@router.post(path="/license")
async def get_details(request: Request,
                      inn: str,
                      # username: str = Depends(dependency=decode_access_token)
                      ):
    # await auth.check_access(username=username, resource_id=settings.UZ_DETAILS_ID, amount=1)
    res, status_code = await make_request(url=settings.UZ_LICENSE_URL.format(inn),
                                          method=request.scope.get("method").lower())
    return JSONResponse(status_code=status_code, content=res, media_type="application/json;charset=utf-8")


@router.post(path="/obnal_tax_gap")
async def get_details(request: Request,
                      inn: str,
                      # username: str = Depends(dependency=decode_access_token)
                      ):
    # await auth.check_access(username=username, resource_id=settings.UZ_DETAILS_ID, amount=1)
    res, status_code = await make_request(url=settings.UZ_OBNAL_TAX_GAP_URL.format(inn),
                                          method=request.scope.get("method").lower())
    return JSONResponse(status_code=status_code, content=res, media_type="application/json;charset=utf-8")


@router.post(path="/tax_debt")
async def get_details(request: Request,
                      inn: str,
                      # username: str = Depends(dependency=decode_access_token)
                      ):
    # await auth.check_access(username=username, resource_id=settings.UZ_DETAILS_ID, amount=1)
    res, status_code = await make_request(url=settings.UZ_TAX_DEBT_URL.format(inn),
                                          method=request.scope.get("method").lower())
    return JSONResponse(status_code=status_code, content=res, media_type="application/json;charset=utf-8")


@router.post(path="/real_estate")
async def get_details(request: Request,
                      inn: str,
                      # username: str = Depends(dependency=decode_access_token)
                      ):
    # await auth.check_access(username=username, resource_id=settings.UZ_DETAILS_ID, amount=1)
    res, status_code = await make_request(url=settings.UZ_REAL_ESTATE_URL.format(inn),
                                          method=request.scope.get("method").lower())
    return JSONResponse(status_code=status_code, content=res, media_type="application/json;charset=utf-8")


@router.post(path="/unfair")
async def get_details(request: Request,
                      search_unfair_body: SearchUnfairBody,
                      # username: str = Depends(dependency=decode_access_token)
                      ):
    # await auth.check_access(username=username, resource_id=settings.UZ_DETAILS_ID, amount=1)
    payload = {
        "company_name": search_unfair_body.company_name,
        "resident_inn": search_unfair_body.resident_inn,
        "resident_name": search_unfair_body.resident_name
    }
    res, status_code = await make_request(url=settings.UZ_UNFAIR_URL,
                                          method=request.scope.get("method").lower(),
                                          json=payload)
    return JSONResponse(status_code=status_code, content=res, media_type="application/json;charset=utf-8")


@router.post(path="/wanted")
async def get_details(request: Request,
                      search_wanted_body: SearchWantedBody,
                      # username: str = Depends(dependency=decode_access_token)
                      ):
    # await auth.check_access(username=username, resource_id=settings.UZ_DETAILS_ID, amount=1)
    payload = {
        "name": search_wanted_body.name,
        "type": search_wanted_body.type
    }
    res, status_code = await make_request(url=settings.UZ_WANTED_URL,
                                          method=request.scope.get("method").lower(),
                                          json=payload)
    return JSONResponse(status_code=status_code, content=res, media_type="application/json;charset=utf-8")
