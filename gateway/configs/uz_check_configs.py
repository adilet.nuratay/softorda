from dotenv import load_dotenv, find_dotenv

load_dotenv(dotenv_path=find_dotenv())


class UZSearchConfig:
    UZ_DETAILS_ID: int = 6
    UZ_SEARCH_URL: str = "http://localhost:8020/search"
    UZ_DETAILS_URL: str = "http://localhost:8020/details"
    UZ_NDS_PAYER_URL: str = "http://localhost:8021/nds_payer?inn={}"
    UZ_LICENSE_URL: str = "http://localhost:8023/license?inn={}"
    UZ_OBNAL_TAX_GAP_URL: str = "http://localhost:8024/obnal_tax_gap?inn={}"
    UZ_TAX_DEBT_URL: str = "http://localhost:8024/tax_debt?inn={}"
    UZ_REAL_ESTATE_URL: str = "http://localhost:8025/real_estate?inn={}"
    UZ_UNFAIR_URL: str = "http://localhost:8026/unfair"
    UZ_WANTED_URL: str = "http://localhost:8026/wanted"
