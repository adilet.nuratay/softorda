import logging

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.middleware.gzip import GZipMiddleware
import uvicorn

from routers.all import router
from config import settings


logging.basicConfig(
    format="%(levelname)s - %(message)s",
    level=logging.INFO
)


app = FastAPI(
    debug=settings.APP_DEBUG,
    title=settings.PROJECT_NAME,
    description=settings.PROJECT_DESCRIPTION,
    version=settings.PROJECT_VERSION,
    root_path=settings.ROOT_PATH
)


app.include_router(router=router)


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
app.add_middleware(GZipMiddleware)


if __name__ == "__main__":
    uvicorn.run(app=app, host='0.0.0.0', port=8000)
