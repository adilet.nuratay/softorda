from http.client import HTTPException
from datetime import datetime, date


from pydantic import BaseModel, validator, root_validator


class SearchUnfairBody(BaseModel):
    company_name: str = ''
    resident_inn: str = ''
    resident_name: str = ''

    @root_validator
    def validate_all_fields(cls, values):
        if not any([values.get('company_name'), values.get('resident_inn'), values.get('resident_name')]):
            raise HTTPException(status_code=400, detail="Empty search criteria!")
        return values


class SearchWantedBody(BaseModel):
    name: str = ''
    type: str = ''

    @root_validator
    def validate_all_fields(cls, values):
        if not any([values.get('name'), values.get('type')]):
            raise HTTPException(status_code=400, detail="Empty search criteria!")
        return values