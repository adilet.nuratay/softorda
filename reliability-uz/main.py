import json
import logging
from logging.handlers import RotatingFileHandler

import redis as redis
import pickle
from fastapi import FastAPI
import uvicorn

from reliability_uz import get_unfair, get_wanted
from schemas import SearchUnfairBody, SearchWantedBody
from config import settings

cache = redis.StrictRedis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.REDIS_DB)

app = FastAPI(title=settings.PROJECT_NAME, root_path=settings.ROOT_PATH)


logging.basicConfig(
    handlers=[RotatingFileHandler("logs/company_search.log", maxBytes=1024 * 1024 * 100, backupCount=20)],
    format="%(asctime)s %(levelname)s:%(message)s",
    level=logging.INFO,
)


@app.post("/unfair")
async def search(search_body: SearchUnfairBody):
    response = get_unfair(search_body)
    return response


@app.post("/wanted")
async def search(search_body: SearchWantedBody):
    response = get_wanted(search_body)
    return response

if __name__ == "__main__":
    uvicorn.run(app, port=5002)
