import logging
import pickle

import redis as redis
from sqlalchemy import and_, or_

from config import settings
from models import Session, UnfairExecutor, Wanted
from schemas import SearchUnfairBody, SearchWantedBody

cache = redis.StrictRedis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.REDIS_DB)
session_db = Session()


def get_unfair(search_body: SearchUnfairBody) -> dict:
    obj = None
    data = {
        'status': None,
        'content': {}
    }
    try:
        if search_body.resident_inn:
            obj = session_db.query(UnfairExecutor).filter(
                UnfairExecutor.resident_inn == search_body.resident_inn).first()
        if search_body.company_name:
            obj = session_db.query(UnfairExecutor).filter(
                UnfairExecutor.name == search_body.company_name).first()
        if search_body.resident_name:
            obj = session_db.query(UnfairExecutor).filter(
                UnfairExecutor.resident_name == search_body.resident_name).first()
        data['content'] = obj if obj else {}
        data['status'] = data['content'] is not None
        return data
    except Exception as msg:
        logging.exception(msg)
        return data


def get_wanted(search_body: SearchWantedBody) -> dict:
    obj = None
    data = {
        'status': None,
        'content': {}
    }
    try:
        obj = session_db.query(Wanted).filter(
            Wanted.full_name.ilike(f'{search_body.name}'),
            and_(Wanted.type == search_body.type)).first()
        data['content'] = obj if obj else {}
        data['status'] = data['content'] is not None
        return data
    except Exception as msg:
        logging.exception(msg)
        return data


if __name__ == "__main__":
    body = SearchUnfairBody(resident_inn='306507834')
    get_unfair(body)
