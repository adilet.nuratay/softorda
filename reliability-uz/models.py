import sqlalchemy
from sqlalchemy import TIMESTAMP, Column, Integer, VARCHAR, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from config import settings

url = "postgresql://{}:{}@{}:{}/{}".format(
    settings.POSTGRES_USER, settings.POSTGRES_PASSWORD, settings.POSTGRES_HOST, settings.POSTGRES_PORT,
    settings.POSTGRES_DB
)
engine = sqlalchemy.create_engine(url, client_encoding="utf8")
Base = declarative_base()
Session = sessionmaker(bind=engine)


class UnfairExecutor(Base):
    __tablename__ = 'unfair_executor'
    id = Column(Integer, primary_key=True, index=True)
    name = Column(VARCHAR(255), comment='Наименование организации.')
    resident_inn = Column(VARCHAR(50), comment='ИНН исполнителя.')
    resident_name = Column(VARCHAR(50), comment='ФИО исполнителя.', nullable=True)
    court_decision_id = Column(VARCHAR(50), comment='Решение суда номер.', nullable=True)
    court_decision_date = Column(TIMESTAMP, comment='Дата решение суда.', nullable=True)
    region_name = Column(VARCHAR(50), comment='', nullable=True)
    date_becoming_dishonest = Column(TIMESTAMP, comment='')
    date_ending_dishonest = Column(TIMESTAMP, comment='')
    created = Column(TIMESTAMP, comment='Дата создания записи.')
    last_updated = Column(TIMESTAMP, comment='Дата обнавления.')


class Wanted(Base):
    __tablename__ = 'wanted'
    id = Column(Integer, primary_key=True, index=True)
    item_id = Column(VARCHAR(50), comment='id.')
    first_name = Column(VARCHAR(50), comment='Имя.', nullable=True)
    last_name = Column(VARCHAR(50), comment='Фамилия.', nullable=True)
    middle_name = Column(VARCHAR(50), comment='Отчество.', nullable=True)
    full_name = Column(VARCHAR(50), comment='Полное имя.', nullable=True)
    type = Column(VARCHAR(50), comment='Тип.', nullable=False)
    description = Column(VARCHAR(255), comment='Описание.', nullable=True)
    initiator = Column(VARCHAR(50), comment='Инициатор.', nullable=True)
    birth_date = Column(VARCHAR(50), comment='Дата рождения.', nullable=True)
    status = Column(Boolean, comment='Статус', nullable=False)
    photo = Column(VARCHAR(255), comment='Фото', nullable=True)
    created = Column(TIMESTAMP, comment='Дата создания записи.')
    last_updated = Column(TIMESTAMP, comment='Дата обнавления.')


Base.metadata.create_all(engine)
