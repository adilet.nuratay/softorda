import os

from dotenv import load_dotenv

load_dotenv()


class Settings:
    PROJECT_NAME: str = os.getenv("PROJECT_NAME", 'Real estate rights UZ')
    ROOT_PATH: str = os.getenv("ROOT_PATH", '')
    PROJECT_VERSION: str = os.getenv("PROJECT_VERSION", '0.1')
    SOURCE_IP: str = os.getenv("SOURCE_IP")
    REAL_ESTATE_BASE_URL: str = os.getenv("REAL_ESTATE_BASE_URL")
    REAL_ESTATE_LOCALE_URL: str = os.getenv("REAL_ESTATE_LOCALE_URL")
    REAL_ESTATE_USER_URL: str = os.getenv("REAL_ESTATE_USER_URL")
    REAL_ESTATE_GET_INFO: str = os.getenv('REAL_ESTATE_GET_INFO')

    REDIS_HOST = os.getenv("REDIS_HOST")
    REDIS_PORT = os.getenv("REDIS_PORT")
    REDIS_DB = os.getenv("REDIS_DB")


os.makedirs('logs', exist_ok=True)

settings = Settings()
