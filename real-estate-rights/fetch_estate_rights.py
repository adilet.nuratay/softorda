import json
import logging

from schemas import RealEstateContentScheme, CadastrSchema, SubjectsSchema
from utils import requests_retry_session
# from urllib.parse import urljoin
session = requests_retry_session()


def fetch_estate_rights(url, method='get',  token=None, payload=None):
    meth = {'get': session.get,
            'post': session.post}
    base_url ='https://davreestr.uz/'
    if token:
        session.headers.update({'X-XSRF-TOKEN': token, 'Referer': base_url})
    r = meth.get(method)(url, params=payload)
    token = session.cookies.get('XSRF-TOKEN').split('%')[0] + '=='
    r.raise_for_status()
    return r, token


def has_numbers(subject):
    return any(char.isdigit() for char in subject)


def get_subjects(subjects):
    '''TODO Delete checking when bug will fixed on source: has_numbers()'''
    subject_obj = {}
    subject_objs = []
    if not subjects:
        return []
    for subject in subjects:
        subject_obj['subject'] = subject if not has_numbers(subject) else ''
        subject_objs.append(SubjectsSchema(**subject_obj))
    return subject_objs


def get_cadastrs(cadastr_list):
    cadastrs = []
    cadastr_obj = {}
    if not cadastr_list:
        return []
    for cadastr in cadastr_list:
        cadastr_obj['cadNumber'] = cadastr.get('cadNumber')
        cadastr_obj['address'] = cadastr.get('cadNumberInfo').get('address')
        cadastr_obj['area'] = cadastr.get('cadNumberInfo').get('area')
        cadastr_obj['land_area'] = cadastr.get('cadNumberInfo').get('land_area')
        cadastr_obj['cost'] = cadastr.get('cadNumberInfo').get('cost')
        cadastr_obj['count_subjects'] = cadastr.get('cadNumberInfo').get('count_subjects')
        cadastr_obj['object_type'] = cadastr.get('cadNumberInfo').get('object_type')
        cadastr_obj['date'] = cadastr.get('cadNumberInfo').get('date')
        cadastr_obj['number'] = cadastr.get('cadNumberInfo').get('number')
        cadastr_obj['has_ban'] = cadastr.get('cadNumberInfo').get('has_ban')
        cadastr_obj['subjects'] = get_subjects(cadastr.get('cadNumberInfo').get('subjects'))
        cadastrs.append(CadastrSchema(**cadastr_obj))
    return cadastrs


def make_response(responce):
    readl_estate = {'org_tin': responce.get('org_tin'),
                    'cadastr_count': responce.get('cadastr_count'),
                    'cadastr_list': get_cadastrs(responce['cadastr_list'])}
    return RealEstateContentScheme(**readl_estate)


def start(inn: str, retries=3):
    try:
        base_url ='https://davreestr.uz/'
        r = session.get(base_url)

        link = 'https://davreestr.uz/locale/ru'
        r, token = fetch_estate_rights(link)

        r = session.get(base_url)

        link = 'https://davreestr.uz/user/inn'
        r, token = fetch_estate_rights(link, token=token)

        link = 'https://davreestr.uz/page/getinfo'
        payload = {
            'org_tin': inn
        }
        r, token = fetch_estate_rights(link, method='post', token=token, payload=payload)
        resource = r.json()
        real_estate_rights = make_response(resource)
        return real_estate_rights
    except Exception as msg:
        logging.exception(msg)
        if retries == 0:
            logging.error('Retry exceeded.')
            return None
        return start(inn, retries=retries-1)


if __name__ == "__main__":
    r = start('200244498')
    print(r)