from datetime import datetime, date
from pydantic import BaseModel, validator, root_validator, ValidationError
from typing import List, Union
from enum import Enum


class SpecializationSchemes(BaseModel):
    position: str
    id: str
    name: str
    description: str


class LicenseContent(BaseModel):
    status: str
    activity_addresses: str
    address: str
    category: str
    document: str
    expiry_date: str
    history: str
    import_register: bool
    is_multi_specialization: bool
    name: str
    number: str
    organization: str
    organization_director: str
    region: str
    specializations: List[SpecializationSchemes] = []
    sub_region: str
    tin: str
    pin: str
    register_number: str
    registration_date: str
    type: str
    uuid: str
    pdf_link: str

    @validator("registration_date", "expiry_date")
    def parse_register_date(cls, value):
        return datetime.strptime(
            value,
            "%d.%m.%Y"
        ).date()

    class Config:
        arbitrary_types_allowed = True


class SubjectsSchema(BaseModel):
    subject: str


class CadastrSchema(BaseModel):
    cadNumber: str
    address: str
    area: float
    land_area: float
    cost: str
    count_subjects: int
    object_type: str
    date: str
    number: str
    has_ban: str
    subjects: List[SubjectsSchema] = []

    @validator("date")
    def parse_register_date(cls, value):
        if value:
            return datetime.strptime(
                value,
                "%Y-%m-%d %H:%M:%S"
            ).date()
        return None

    class Config:
        arbitrary_types_allowed = True

    @validator("object_type")
    def parse_object_type(cls, value):
        if value == '1':
            return 'Жилой'
        if value == '2':
            return 'Нежилой'

    class Config:
        arbitrary_types_allowed = True


class RealEstateContentScheme(BaseModel):
    org_tin: str
    cadastr_count: int
    cadastr_list: List[CadastrSchema] = []


class RealEstateScheme(BaseModel):
    status: Union[bool, None]
    content: RealEstateContentScheme = {}
    # content: List[LicenseContent] = {}

