import json
import logging
import pickle
from logging.handlers import RotatingFileHandler

from fastapi import FastAPI
import uvicorn
import redis

import fetch_estate_rights
from config import settings
from schemas import RealEstateScheme


cache = redis.StrictRedis(
    host=settings.REDIS_HOST,
    port=settings.REDIS_PORT,
    db=settings.REDIS_DB,
)

app = FastAPI(
    title=settings.PROJECT_NAME,
    root_path=settings.ROOT_PATH
)

logging.basicConfig(
    handlers=[RotatingFileHandler("logs/license_search.log", maxBytes=1024 * 1024 * 100, backupCount=20)],
    format="%(asctime)s %(levelname)s:%(message)s",
    level=logging.INFO,
)


@app.post("/real_estate", tags=['Search'])
async def search(inn: str):
    key = f"{inn}_real_estate"
    from_redis = cache.get(key)
    if from_redis:
        content = pickle.loads(from_redis)
    else:
        content = fetch_estate_rights.start(inn)
        pickled_object = pickle.dumps(content)
        cache.set(key, pickled_object)
    if content is None:
        return RealEstateScheme(status=None)

    if content == {}:
        return RealEstateScheme(status=False)
    return RealEstateScheme(status=True, content=content)


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
