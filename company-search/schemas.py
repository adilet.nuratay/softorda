from datetime import datetime, date
from typing import List, Union

from fastapi.exceptions import HTTPException
from pydantic import BaseModel, validator, root_validator


class Executive(BaseModel):
    name: str
    inn: str


class Founder(BaseModel):
    name: str
    share: str


class Contacts(BaseModel):
    email: str
    phone_number: str
    soato: str
    address: str


class CompanyContent(BaseModel):
    inn: str
    registration_organ: str
    register_date: date
    register_number: str
    full_name: str
    short_name: str
    kopf: str
    ownership: str
    oked: str
    soogu: str
    small_business: bool
    is_active: bool
    authorized_fund: str
    executive: Executive
    contacts: Contacts
    founders: List[Founder]

    @validator("register_date", pre=True)
    def parse_register_date(cls, value):
        return datetime.strptime(
            value,
            "%d.%m.%Y"
        ).date()

    @validator("small_business", pre=True)
    def parse_small_business(cls, value):
        if value == 'Да':
            return True
        else:
            return False

    @validator("is_active", pre=True)
    def parse_is_active(cls, value):
        if value == 'Действующий':
            return True
        else:
            return False


class CompanySchema(BaseModel):
    status: Union[bool, None]
    content: CompanyContent = {}


class SearchBody(BaseModel):
    name: str = ''
    inn: str = ''
    pinfl: str = ''

    @root_validator
    def validate_all_fields(cls, values):
        if not any([values.get('name'), values.get('inn'), values.get('pinfl')]):
            raise HTTPException(status_code=400, detail="Empty search criteria!")
        return values


class IdentifierSchema(BaseModel):
    type: str
    value: str


class IndustryCodeSchema(BaseModel):
    is_main: bool
    code: str
    classification: str
    descr: str


class AddressSchema(BaseModel):
    state: str
    street: str


class OfficerSchema(BaseModel):
    identifier: str
    name: str
    role: str
    inactive: bool


class MainSearchCompanySchema(BaseModel):
    title: str
    identifiers: List[IdentifierSchema]
    main_industry_code: str
    industry_code: IndustryCodeSchema
    name: str
    name_en: str
    name_native: str
    addresses: List[AddressSchema]
    headcount: List[dict]
    industry: str
    officers: List[OfficerSchema]


class MainSearchSchema(BaseModel):
    status: Union[bool, None]
    content: List[MainSearchCompanySchema]
