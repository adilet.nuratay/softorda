import requests
import logging

from twocaptchaapi import TwoCaptchaApi
from requests.adapters import HTTPAdapter
from requests_toolbelt.adapters import source
from urllib3.exceptions import InsecureRequestWarning
from urllib3.util import Retry
import urllib3

from config import settings

urllib3.disable_warnings(InsecureRequestWarning)
api = TwoCaptchaApi(settings.CAPTCHA_KEY)
SOURCE_IP = None


def requests_retry_session(
        retries=5,
        backoff_factor=1,
        status_forcelist=(400, 500, 502, 503, 504),
        session=None,
):
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist,
    )
    if SOURCE_IP:
        adapter = source.SourceAddressAdapter(SOURCE_IP, max_retries=retry)
    else:
        adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session


def solve_base64(content, report=False, captcha_parameters=None, retry=0):
    try:
        c = api.solve(content, captcha_parameters=captcha_parameters)
        result = c.await_result()
        if report:
            logging.warning(f'[CAPTCHA] {report}')
            return result
        else:
            return result
    except Exception as msg:
        logging.exception(msg)
        if retry > 3:
            logging.warning("CAPTCHA FAILED")
            return
        solve_base64(content, report, captcha_parameters, retry+1)
