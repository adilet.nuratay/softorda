import io
import logging
from typing import Union

from bs4 import BeautifulSoup

from utils import requests_retry_session, solve_base64
from schemas import CompanyContent, SearchBody
from config import settings

def parse_common(trs):
    mapping = {
        'ИНН': 'inn',
        'Регистрирующий орган': 'registration_organ',
        'Дата государственной регистрации': 'register_date',
        'Номер регистрации в реестре': 'register_number',
        'Полное наименование': 'full_name',
        'Сокращенное наименование': 'short_name',
        'Организационно-правовая форма (ОПФ)': 'kopf',
        'Форма собственности (ФС)': 'ownership',
        'Код ОКЭД (Вид(ы) осуществляемой деятельности)': 'oked',
        'Код СООГУ': 'soogu',
        'Принадлежность к субъектам малого': 'small_business',
        'Состояние деятельности предприятия': 'is_active',
        'Уставный фонд': 'authorized_fund',
    }
    result = {}
    for tr in trs:
        tds = tr.find_all("td")
        key = tds[0].text.strip(" \n").rstrip(" \n")
        value = tds[1].text.strip(" \n").rstrip(" \n,").replace("\n", " ")
        result[mapping.get(key, key)] = value
    return result


def parse_executive(trs):
    mapping = {
        'Имя руководителя': 'name',
        'ИНН руководителя': 'inn'
    }
    result = {}
    for tr in trs:
        tds = tr.find_all("td")
        key = tds[0].text.strip(" \n").rstrip(" \n")
        value = tds[1].text.strip(" \n").rstrip(" \n,").replace("\n", " ")
        result[mapping.get(key, key)] = value
    return result


def parse_founders(trs):
    result = []
    for tr in trs:
        tds = tr.find_all("td")
        key = tds[0].text.strip(" \n").rstrip(" \n")
        value = tds[1].text.strip(" \n").rstrip(" \n,").replace("\n", " ")
        result.append({
            'name': key,
            'share': value
        })
    return result


def parse_contacts(trs):
    mapping = {
        'Адрес электронной почты': 'email',
        'Телефон': 'phone_number',
        'Код СОАТО': 'soato',
        'Местонахождение': 'address'
    }
    result = {}
    for tr in trs:
        tds = tr.find_all("td")
        key = tds[0].text.strip(" \n").rstrip(" \n")
        value = tds[1].text.strip(" \n").rstrip(" \n,").replace("\n", " ")
        result[mapping.get(key, key)] = value
    return result


def parse_response(page):
    div_info = page.find("div", {"class": "col-md-8"})
    if div_info is None:
        return {}
    children_div_info = div_info.findChildren(recursive=False)
    data = {}
    for i in range(0, len(children_div_info), 2):
        name_block = children_div_info[i].text
        trs = children_div_info[i + 1].find_all("tr")
        if name_block == 'Общие сведения':
            data = parse_common(trs)
        if name_block == 'Информация о руководителе':
            data['executive'] = parse_executive(trs)
        if name_block == 'Информация об учредителях и их доле в уставном фонде':
            data['founders'] = parse_founders(trs)
        if name_block == 'Контактные данные':
            data['contacts'] = parse_contacts(trs)
    return data


def solve_captcha(session, page, url):
    link_captcha = page.find("img", {"class": "qr-code"}).get("src")
    captcha_url = url.split("/pub")[0] + link_captcha
    r = session.get(url=captcha_url, timeout=15)
    return solve_base64(io.BytesIO(r.content), captcha_parameters={"regsense": 1}, )


def get_company(inn: str, retries=3) -> dict:
    data = {'status': None, 'content': {}}
    try:
        logging.info(f"Company Search start: {inn}")
        url = settings.FETCH_URL
        headers = {
            'User-Agent': "Mozilla/5.0 (X11; CrOS x86_64 12871.102.0) "
                          "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.141 Safari/537.36})"
        }
        session = requests_retry_session()
        session.headers.update(headers)
        session.verify = False
        r = session.get(url=url, timeout=15)
        main_page = BeautifulSoup(r.content.decode("utf8"), "html.parser")
        captcha = solve_captcha(session, main_page, url)

        csrf = main_page.find("input", {"name": "_csrf"}).get("value")
        payload = {
            "name": '',
            "tin": inn,
            "pinfl": '',
            "answer": captcha,
            "_csrf": csrf,
        }
        r = session.post(url=url, data=payload, timeout=15)
        response_page = BeautifulSoup(r.content.decode("utf8"), "html.parser")
        if 'Неверный код подтверждения' in response_page.text:
            raise Exception('Captcha not solved')
        elif 'По наборному Инн не найдено данных' in response_page.text:
            content = {}
        else:
            content = parse_response(response_page)
        data['content'] = content
        data['status'] = data['content'] != {}
        return data
    except Exception as e:
        logging.exception(e)
        if retries == 0:
            print(retries)
            return data
        else:
            get_company(inn, retries=retries - 1)


if __name__ == "__main__":
    # body = SearchBody(name='global')
    res = get_company(inn='304749109')
    print(res)
