import logging
import redis
import pickle
from celery import Celery
from config import settings
import fetch_company
# from schemas import SearchBody

app = Celery('task', broker=settings.BROKER_URL, backend=settings.BACKEND_URL)

cache = redis.StrictRedis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.REDIS_DB)


@app.task(queue='details')
def details(key_value, key_status, inn):
    logging.info(f'Start task {inn}')
    print(f'Start task {inn}')
    data = fetch_company.get_company(inn)
    if data:
        if data.get('status') is True:
            logging.info(f"stats true {data['status']}")
            print(f"stats true {data['status']}")
            logging.info(f'data: {data}')
            cache.set(key_status, 'YES', ex=86400 * 4)
            pickled_object = pickle.dumps(data.get('content'))
            cache.set(key_value, pickled_object, ex=86400 * 4)
        elif data.get('status') is None:
            logging.info(f"stats None {data['status']}")
            print(f"stats true {data['status']}")
            cache.set(key_status, 'NULL', ex=10)
        elif data.get('status') is False:
            logging.info(f"stats False {data['status']}")
            print(f"stats true {data['status']}")
            cache.set(key_status, 'NO', ex=10)
    else:
        logging.info(f"stats None")
        cache.set(key_status, 'NULL', ex=10)
    return True


if __name__ == "__main__":
     details(key_value="201036454_value", key_status="201036454_status", inn="201036454")
