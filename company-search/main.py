import logging
from logging.handlers import RotatingFileHandler
import pickle

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import redis as redis
import uvicorn

from schemas import SearchBody
import search_company
from config import settings
from task import tasks


cache = redis.StrictRedis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.REDIS_DB)

app = FastAPI(title=settings.PROJECT_NAME, root_path=settings.ROOT_PATH)

logging.basicConfig(
    handlers=[RotatingFileHandler("logs/search.log", maxBytes=1024 * 1024 * 100, backupCount=20)],
    format="%(asctime)s %(levelname)s:%(message)s",
    level=logging.INFO,
)

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.post("/details")
async def search(search_body: SearchBody):
    data = {'status': None, 'content': {}}
    key_status = f'{search_body.inn}_status'
    print(key_status)
    key_value = f'{search_body.inn}_value'
    print(key_value)
    from_redis = cache.get(key_status)
    if from_redis:
        status = from_redis.decode()
        print(f'status {status}')
        if status == 'YES':
            data['status'] = 'YES'
            data['content'] = pickle.loads(cache.get(key_value))
            return data
        elif status == 'NO':
            print(f'status {status}')
            data['status'] = 'NO'
            return data
        elif status == 'NULL':
            print(f'status {status}')
            data['status'] = 'SYNC'
            cache.set(key_status, 'SYNC')
            tasks.details.delay(key_value, key_status, search_body.inn)
            return data
        elif status == 'SYNC':
            print(f'status {status}')
            data['status'] = 'SYNC'
            return data
    else:
        cache.set(key_status, 'SYNC')
        tasks.details.delay(key_value, key_status, search_body.inn)
        data['status'] = 'SYNC'
        return data


@app.post("/search")
async def search(search_body: SearchBody):
    key = str(search_body)
    print(key)
    response = cache.get(key)
    print(f'response {response}')
    if response:
        response = pickle.loads(cache.get(key))
    else:
        response = search_company.start(search_body)
        if response.status is not True:
            return response
    pickled_object = pickle.dumps(response)
    cache.set(key, pickled_object, ex=600)
    return response


if __name__ == "__main__":
    uvicorn.run(app, port=8000)
