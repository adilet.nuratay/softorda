import logging

import requests
from requests import Session

from utils import requests_retry_session
from schemas import MainSearchSchema, SearchBody
from config import settings


def get_manifest_key(session: Session) -> str:
    url = settings.MANIFEST_URL
    headers = {
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'accept-encoding': 'gzip, deflate',
        'accept-language': 'en-US,en;q=0.9',
        'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"macOS"',
        'sec-fetch-dest': 'document',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-user': '?1',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36'
    }
    try:
        r = session.get(url=url, headers=headers, verify=False)
        page_text = r.text
        page_text = page_text[:page_text.find('/_buildManifest.js')]
        manifest_key = page_text[page_text.rfind('/') + 1:]
        return manifest_key
    except Exception as e:
        logging.exception(e)
        return ''


def search(search_value: str) -> dict:
    session = requests_retry_session()
    manifest_key = get_manifest_key(session)
    data = {
        'status': None,
        'content': []
    }
    if not manifest_key:
        return data
    url = settings.SEARCH_URL.format(manifest_key, search_value, search_value)
    headers = {
        'accept': '*/*',
        'accept-encoding': 'gzip, deflate',
        'accept-language': 'en-US,en;q=0.9',
        'referer': settings.SEARCH_REFERER.format(search_value),
        'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"macOS"',
        'sec-fetch-dest': 'empty',
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'same-origin',
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
        'x-nextjs-data': '1'
    }
    try:
        r = session.get(url=url, headers=headers, verify=False)
        data['content'] = r.json()['pageProps']['companies']
        data['status'] = len(data['content']) > 0
    except Exception as e:
        logging.exception(e)
    finally:
        session.close()
        return data


def start(search_body: SearchBody) -> MainSearchSchema:
    print(f'start func {search_body}')
    try:
        search_value = search_body.inn
        if search_body.name:
            search_value = search_body.name
        if search_body.pinfl:
            search_value = search_body.pinfl
        result = search(search_value)
        print(result)
        response = MainSearchSchema(**result)
        return response
    except Exception as e:
        logging.exception(e)


if __name__ == '__main__':
    res = start(SearchBody(inn='304749109'))
    print(res.json())
