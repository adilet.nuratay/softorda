import os

from dotenv import load_dotenv

load_dotenv()


class Settings:
    PROJECT_NAME = os.getenv("PROJECT_NAME", 'Company Search UZ')
    ROOT_PATH = os.getenv("ROOT_PATH", '')

    CAPTCHA_KEY = os.getenv("CAPTCHA_KEY")

    REDIS_HOST = os.getenv("REDIS_HOST")
    REDIS_PORT = os.getenv("REDIS_PORT")
    REDIS_DB = os.getenv("REDIS_DB")

    BROKER_URL = os.getenv("BROKER_URL")
    BACKEND_URL = os.getenv("BACKEND_URL")

    FETCH_URL = os.getenv('FETCH_URL')
    MANIFEST_URL = os.getenv('MANIFEST_URL')
    SEARCH_URL = os.getenv('SEARCH_URL')
    SEARCH_REFERER = os.getenv('SEARCH_REFERER')


settings = Settings()

os.makedirs('logs', exist_ok=True)


