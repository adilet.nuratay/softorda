from datetime import datetime, date
from pydantic import BaseModel, validator, root_validator, ValidationError
from typing import List, Union
from enum import Enum


class SpecializationSchemes(BaseModel):
    position: str
    id: str
    name: str
    description: str


class LicenseContent(BaseModel):
    status: str
    activity_addresses: str
    address: str
    category: str
    document: str
    expiry_date: str
    history: str
    import_register: bool
    is_multi_specialization: bool
    name: str
    number: str
    organization: str
    organization_director: str
    region: str
    specializations: List[SpecializationSchemes] = []
    sub_region: str
    tin: str
    pin: str
    register_number: str
    registration_date: str
    type: str
    uuid: str
    pdf_link: str

    @validator("registration_date", "expiry_date")
    def parse_register_date(cls, value):
        return datetime.strptime(
            value,
            "%d.%m.%Y"
        ).date()

    class Config:
        arbitrary_types_allowed = True


class ContentSchema(BaseModel):
    uz: LicenseContent = {}
    ru: LicenseContent = {}
    en: LicenseContent = {}


class LicenseScheme(BaseModel):
    status: Union[bool, None]
    content: List[ContentSchema] = {}


class LangEnum(str, Enum):
    RU = "ru"
    EN = "en"
    UZ = "uz"


class SearchBody(BaseModel):
    inn: str
    lang: LangEnum

    @validator("inn")
    def parse_inn(cls, value):
        if isinstance(value, str):
            return value
        else:
            ValidationError('inn must be str.')
