import logging

import requests

from schemas import ContentSchema, LicenseContent, SpecializationSchemes, LangEnum
from utils import requests_retry_session
from config import settings
headers = {
    'User-Agent': "Mozilla/5.0 (X11; CrOS x86_64 12871.102.0) "
                  "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.141 Safari/537.36})"
}


def get_specializations(specializations, lang):
    if len(specializations) > 0:
        specializations_collections = []
        specialization_obj = {}
        for specialization in specializations:
            specialization_obj['position'] = specialization.get('position') if specialization.get('position') else ''
            specialization_obj['id'] = str(specialization.get('id'))
            specialization_obj['name'] = specialization.get('name').get(lang)
            specialization_obj['description'] = specialization.get('description')\
                .get(lang) if specialization.get('description').get(lang) else ''
            specializations_collections.append(SpecializationSchemes(**specialization_obj))
        return specializations_collections
    else:
        return []


def get_pdf_url(uuid, lang):
    pdf_url = settings.LICENSE_PDF_URL.format(uuid, lang)
    r = requests.get(pdf_url)
    content = r.content
    if isinstance(content, bytes):
        return pdf_url


def get_certificate(cert, lang: LangEnum):
    license = {}
    license['status'] = cert.get('status').get('status')
    try:
        license['activity_addresses'] = cert.get('activity_addresses')[0].get('value').get(lang)
    except:
        license['activity_addresses'] = None
    license['address'] = '{}, {}, {}'.format(cert.get('region').get(lang),
                                             cert.get('subRegion').get(lang),
                                             cert.get('address'))
    license['category'] = cert.get('category').get('title').get(lang)
    license['document'] = cert.get('document').get(lang)
    license['expiry_date'] = cert.get('expiry_date')
    license['history'] = cert.get('history') if cert.get('history') is not None else ''
    license['import_register'] = cert.get('import_register')
    license['is_multi_specialization'] = cert.get('is_multi_specialization')
    license['name'] = cert.get('name')
    license['number'] = cert.get('number')
    license['organization'] = cert.get('organization').get(lang)
    license['organization_director'] = cert.get('organization_director').get(lang)
    license['region'] = cert.get('region').get(lang)
    license['specializations'] = get_specializations(cert.get('specializations'), lang)
    license['sub_region'] = cert.get('subRegion').get(lang)
    license['tin'] = cert.get('tin')
    license['pin'] = cert.get('pin')
    license['register_number'] = cert.get('register_number')
    license['registration_date'] = cert.get('registration_date')
    license['type'] = cert.get('type')
    license['uuid'] = cert.get('uuid')
    license['pdf_link'] = get_pdf_url(cert['uuid'], lang)
    # license_obj = LicenseContent(**license)
    license_obj = license
    return license_obj


def get_license(session, url, inn):
    license_schema = []
    payload = {
        'tin': inn,
        'page': '0',
        'size': '10'
    }
    r = session.get(url=url, params=payload)
    r.raise_for_status()
    resource = r.json()
    if resource.get('status_message') != 'OK' and len(resource.get('data').get('certificates')) <= 0:
        logging.info(f'{inn} inn not found')
        return license_schema

    certificates = resource.get('data').get('certificates')
    for cert in certificates:
        content_schema_obj = ContentSchema()
        license_obj = get_certificate(cert, LangEnum.RU)
        content_schema_obj.ru = license_obj
        license_obj = get_certificate(cert, LangEnum.EN)
        content_schema_obj.en = license_obj
        license_obj = get_certificate(cert, LangEnum.UZ)
        content_schema_obj.uz = license_obj
        license_schema.append(content_schema_obj)
    return license_schema


def start(inn: str, retries=3):
    try:
        main_url = settings.LICENSE_MAIN_URL
        page_url = settings.LICENSE_PAGE_URL
        session = requests_retry_session()
        session.headers.update(headers)
        r = session.get(url=main_url, verify=False)
        r.raise_for_status()
        licenses = get_license(session, page_url, inn)
        return licenses
    except Exception as e:
        logging.exception(e)
        if retries == 0:
            logging.error('License Search failed: inn {}'.format(inn))
            return None
        return start(inn, retries=retries - 1)


if __name__ == "__main__":
    print(start(inn='301250718'))
