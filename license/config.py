import os

from dotenv import load_dotenv

load_dotenv()


class Settings:
    PROJECT_NAME: str = os.getenv("PROJECT_NAME", 'License.gov.uz-registry UZ')
    ROOT_PATH: str = os.getenv("ROOT_PATH", '')
    SOURCE_IP: str = os.getenv("SOURCE_IP")
    LICENSE_MAIN_URL: str = os.getenv("LICENSE_MAIN_URL")
    LICENSE_PAGE_URL: str = os.getenv("LICENSE_PAGE_URL")
    LICENSE_PDF_URL: str = os.getenv('LICENSE_PDF_URL')

    REDIS_HOST = os.getenv("REDIS_HOST")
    REDIS_PORT = os.getenv("REDIS_PORT")
    REDIS_DB = os.getenv("REDIS_DB")


os.makedirs('logs', exist_ok=True)

settings = Settings()
