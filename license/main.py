import json
import logging
from logging.handlers import RotatingFileHandler
import pickle

from fastapi import FastAPI
import uvicorn
import redis

import license
from schemas import LicenseScheme
from config import settings


cache = redis.StrictRedis(
    host=settings.REDIS_HOST,
    port=settings.REDIS_PORT,
    db=settings.REDIS_DB
)

app = FastAPI(
    title=settings.PROJECT_NAME,
    root_path=settings.ROOT_PATH
)

logging.basicConfig(
    handlers=[RotatingFileHandler("logs/license_search.log", maxBytes=1024 * 1024 * 100, backupCount=20)],
    format="%(asctime)s %(levelname)s:%(message)s",
    level=logging.INFO,
)


@app.post("/license", tags=['Search'])
async def search(inn: str):
    key = f"{inn}_license"
    from_redis = cache.get(key)
    if from_redis:
        content = pickle.loads(from_redis)
    else:
        content = license.start(inn)
        if content is None:
            return LicenseScheme(status=None)
        pickled_object = pickle.dumps(content)
        cache.set(key, pickled_object, ex=30600)
    if content == {}:
        return LicenseScheme(status=False)
    return LicenseScheme(status=True, content=content)


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=5000)
