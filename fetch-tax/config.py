import os

from dotenv import load_dotenv

load_dotenv()


class Settings:
    PROJECT_NAME: str = os.getenv("PROJECT_NAME", 'Fetch tax by INN UZ')
    ROOT_PATH: str = os.getenv("ROOT_PATH", '')
    SOURCE_IP: str = os.getenv("SOURCE_IP")

    CAPTCHA_KEY: str = os.getenv("CAPTCHA_KEY")

    DEBIT_MAIN_URL: str = os.getenv("DEBIT_MAIN_URL")
    DEBIT_ITEM_URL: str = os.getenv("DEBIT_ITEM_URL")
    OBNAL_MAIN_URL: str = os.getenv("OBNAL_MAIN_URL")
    OBNAL_DATA_URL: str = os.getenv("OBNAL_DATA_URL")
    OBNAL_ITEM_URL: str = os.getenv("OBNAL_ITEM_URL")
    TAX_CAPTCH_URL: str = os.getenv('TAX_CAPTCH_URL')

    REDIS_HOST = os.getenv("REDIS_HOST")
    REDIS_PORT = os.getenv("REDIS_PORT")
    REDIS_DB = os.getenv("REDIS_DB")

os.makedirs('logs', exist_ok=True)

settings = Settings()
