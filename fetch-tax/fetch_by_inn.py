import base64
import io
import json
import logging

import redis

from utils import requests_retry_session, solve_base64
from schemas import TaxDebtContent, ObnalContent
from config import settings
headers = {
    'User-Agent': "Mozilla/5.0 (X11; CrOS x86_64 12871.102.0) "
                  "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.141 Safari/537.36})"
}


cache = redis.StrictRedis(
    host=settings.REDIS_HOST,
    port=settings.REDIS_PORT,
    db=settings.REDIS_DB
)

def solve_captcha(session):
    captcha_url = settings.TAX_CAPTCH_URL
    r = session.get(url=captcha_url, timeout=15)
    b_img = r.json()['imgSrc'].split(', ')[-1].encode()
    b64_img = base64.b64decode(b_img)
    return solve_base64(io.BytesIO(b64_img))


def get_obnal(session, item_url, captcha,  inn):
    obnal = {}
    payload = {
        'tin': inn,
        'captcha': captcha
    }
    r = session.get(url=item_url, params=payload)
    response = r.json()
    if response['success']:
        obnal['status'] = response.get('data').get('status')
        obnal['sum_debt'] = str(response.get('data').get('sumDebt'))
    return obnal


def get_page(session, data_url, inn):
    tax_gap = {}
    payload = {
        'tin': inn,
        'captcha': '2382'
    }
    r = session.get(url=data_url, params=payload)
    response = r.json()
    if response['success']:
        data = response.get('data')
        tax_gap['tin'] = data.get('tin') if data.get('tin') else ''
        tax_gap['name'] = data.get('name') if data.get('name') else ''
        tax_gap['state_name'] = data.get('stateName') if data.get('stateName') else ''
        tax_gap['address'] = data.get('address') if data.get('address') else ''
        tax_gap['register_date'] = data.get('regDate') if data.get('regDate') else ''
        tax_gap['register_number'] = data.get('regNum') if data.get('regNum') else ''
        tax_gap['soato_code'] = data.get('nc5Name') if data.get('nc5Name') else ''
        tax_gap['opf_code'] = data.get('nc4Name') if data.get('nc4Name') else ''
        tax_gap['oked'] = data.get('nc6Name') if data.get('nc6Name') else ''
        tax_gap['soogu'] = data.get('nc1Name') if data.get('nc1Name') else ''
        tax_gap['tax_gap'] = data.get('taxGap') if data.get('taxGap') else ''
        tax_gap['vat_state'] = data.get('vatState') if data.get('vatState') else ''
    return tax_gap


def get_soliq_session(url):
    session = requests_retry_session()
    session.headers.update(headers)
    r = session.get(url=url, timeout=15)
    r.raise_for_status()
    return session


def get_tax_debt(session, item_url, captcha, inn):
    tax_debt = {}
    payload = {
        'tin': inn,
        'captcha': captcha
    }
    r = session.post(url=item_url, data=payload)
    r.raise_for_status()
    response = r.json()
    if response['success']:
        data = response.get('data')
        tax_debt['tin'] = data.get('tin')
        tax_debt['inspection_body'] = data.get('ns10Name')
        tax_debt['name'] = data.get('name')
        tax_debt['status'] = data.get('status')
        tax_debt['sum_debt'] = data.get('sumDebt')
    return tax_debt


def fetch_tax_gap_obnal(inn: str, retries=3):
    logging.info(f"Obnal Tax start: {inn}")
    try:
        key = f"{inn}_tax_gap"
        from_redis = cache.get(key)
        if from_redis:
            content = json.loads(from_redis)
        else:
            main_url = settings.OBNAL_MAIN_URL
            data_url = settings.OBNAL_DATA_URL
            item_url = settings.OBNAL_ITEM_URL
            session = get_soliq_session(main_url)
            captcha = solve_captcha(session)
            obnal = get_obnal(session, item_url, captcha, inn)
            content = get_page(session, data_url, inn)
            content['obnal'] = obnal
            cache.set(key, json.dumps(content, ensure_ascii=False), ex=30600)
        return ObnalContent(**content)
    except Exception as e:
        logging.exception(e)
        if retries == 0:
            logging.error(f'Company Search failed: {inn}')
            return None
        return fetch_tax_gap_obnal(inn, retries=retries-1)


def fetch_tax_debit(inn: str, retries=3):
    try:
        key = f"{inn}_tax_debt"
        from_redis = cache.get(key)
        if from_redis:
            tax_debt = json.loads(from_redis)
        else:
            main_url = settings.DEBIT_MAIN_URL
            item_url = settings.DEBIT_ITEM_URL
            session = get_soliq_session(main_url)
            captcha = solve_captcha(session)
            tax_debt = get_tax_debt(session, item_url, captcha, inn)
            cache.set(key, json.dumps(tax_debt, ensure_ascii=False), ex=30600)
        return TaxDebtContent(**tax_debt)
    except Exception as e:
        logging.exception(e)
        if retries == 0:
            logging.error(f'Tax debt Search failed: {inn}')
            return None
        return fetch_tax_gap_obnal(inn, retries=retries-1)


if __name__ == "__main__":
    # body = SearchBody(inn='201170210')
    # body = SearchBody(inn='306249071')
    # fetch_tax_gap_obnal(body)
    r = fetch_tax_debit(inn='200244767')
    # fetch_tax_gap_obnal(inn='200244767')
    print(r)
