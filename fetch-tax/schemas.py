from datetime import datetime, date
from typing import Union

from pydantic import BaseModel, validator

status_code = {
    2: 'Сомнительный'
}


class Obnal(BaseModel):
    status: str
    sum_debt: str

    @validator("status", pre=True)
    def validate_status(cls, value):
        if value is None or value == '':
            return ''
        return status_code[value]


class ObnalContent(BaseModel):
    tin: str
    state_name: str
    register_date: date
    register_number: str
    name: str
    address: str
    soato_code: str
    opf_code: str
    oked: str
    soogu: str
    tax_gap: str
    vat_state: bool
    obnal: Union[Obnal, dict] = {}

    @validator("register_date", pre=True)
    def parse_register_date(cls, value):
        return datetime.strptime(
            value,
            "%d.%m.%Y"
        ).date()

    @validator("vat_state", pre=True)
    def parse_vat_state(cls, value):
        if value is None:
            return False
        else:
            return True


class ObnalTaxSchema(BaseModel):
    status: Union[bool, None]
    content: ObnalContent = {}


class TaxDebtContent(BaseModel):
    tin: str
    inspection_body: str
    name: str
    status: str
    sum_debt: str

    @validator("status", pre=True)
    def validate_status(cls, value):
        if value is None or value == '':
            return ''
        return status_code[value]


class TaxDebtSchema(BaseModel):
    status: Union[bool, None]
    content: TaxDebtContent = {}
