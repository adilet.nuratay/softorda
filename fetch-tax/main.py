import json
import logging
from logging.handlers import RotatingFileHandler

from fastapi import FastAPI
import uvicorn
import redis

from config import settings
from fetch_by_inn import fetch_tax_gap_obnal, fetch_tax_debit
from schemas import ObnalTaxSchema, TaxDebtSchema


app = FastAPI(
    title=settings.PROJECT_NAME,
    root_path=settings.ROOT_PATH
)

logging.basicConfig(
    handlers=[RotatingFileHandler("logs/taxes.log", maxBytes=1024 * 1024 * 100, backupCount=20)],
    format="%(asctime)s %(levelname)s:%(message)s",
    level=logging.INFO,
)


@app.post("/obnal_tax_gap")
async def obnal_tax_gap(inn: str):
    content = fetch_tax_gap_obnal(inn)
    if content is None:
        return ObnalTaxSchema(status=None)
    if content == {}:
        return ObnalTaxSchema(status=False)
    return ObnalTaxSchema(status=True, content=content)


@app.post("/tax_debt")
async def tax_debt(inn: str):
    content = fetch_tax_debit(inn)
    if content is None:
        return TaxDebtSchema(status=None)
    if content == {}:
        return TaxDebtSchema(status=False)
    return TaxDebtSchema(status=True, content=content)


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=5001)
