from datetime import datetime, date
from pydantic import BaseModel, validator


class NdsPayerContent(BaseModel):
    inn: str
    date_from: date
    date_reg: date
    reg_code: str
    state_text: str

    @validator("date_from", "date_reg", pre=True)
    def parse_register_date(cls, value):
        return datetime.strptime(
            value,
            "%d.%m.%Y"
        ).date()


class NdsPayerResponse(BaseModel):
    status: bool = None
    content: NdsPayerContent = {}
