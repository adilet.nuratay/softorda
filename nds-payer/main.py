import logging
from logging.handlers import RotatingFileHandler
import json

from fastapi import FastAPI
import uvicorn
import redis

import fetch_nds_payer
from config import settings
from schemas import NdsPayerResponse


app = FastAPI(
    title=settings.PROJECT_NAME,
    root_path=settings.ROOT_PATH
)

logging.basicConfig(
    handlers=[RotatingFileHandler("logs/nds_payer.log", maxBytes=1024 * 1024 * 100, backupCount=20)],
    format="%(asctime)s %(levelname)s:%(message)s",
    level=logging.INFO,
)


@app.post("/nds_payer", tags=['Search'])
async def search(inn: str):
    content = fetch_nds_payer.start(inn)
    if content is None:
        return NdsPayerResponse(status=None)
    if content == {}:
        return NdsPayerResponse(status=False)
    return NdsPayerResponse(status=True, content=content)


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=5000)
