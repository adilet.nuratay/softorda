import json
import logging
from typing import Union

import redis

from utils import requests_retry_session
from schemas import NdsPayerContent
from config import settings


cache = redis.StrictRedis(
    host=settings.REDIS_HOST,
    port=settings.REDIS_PORT,
    db=settings.REDIS_DB
)


def parse_data(data: dict) -> Union[NdsPayerContent, dict]:
    if not data.get('recordsTotal'):
        return {}
    nds_payer = data['data'][0]
    content = {
        'inn': nds_payer.get('tin'),
        'date_from': nds_payer.get('dateFrom'),
        'date_reg': nds_payer.get('dateReg'),
        'reg_code': nds_payer.get('regCode'),
        'state_text': nds_payer.get('stateText')
    }
    return content


def start(inn: str):
    key = f"{inn}_nds_payer"
    from_redis = cache.get(key)
    if from_redis:
        content = json.loads(from_redis)
    else:
        url = settings.NDS_MAIN_URL
        headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.2 Safari/605.1.15',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Accept': 'application/json, text/javascript, */*; q=0.01',
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'Accept-Encoding': 'gzip, deflate, br',
            'Host': 'my.soliq.uz',
            'Origin': settings.NDS_ORIGIN,
            'Referer': settings.NDS_REFERER,
            'Cookie': 'lang=ru'
        }
        payload = {
            'MIME Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'sEcho': '2',
            'iColumns': '10',
            'sColumns': ',,,,,,,,,',
            'iDisplayStart': '0',
            'iDisplayLength': '10',
            'mDataProp_0': 'rn',
            'mDataProp_1': 'ns10Name',
            'mDataProp_2': 'ns11Name',
            'mDataProp_3': 'name',
            'mDataProp_4': 'tin',
            'mDataProp_5': 'pinfl',
            'mDataProp_6': 'dateFrom',
            'mDataProp_7': 'dateReg',
            'mDataProp_8': 'regCode',
            'mDataProp_9': 'stateText',
            'ns10Code': '0',
            'ns11Code': '0',
            'searchText': inn,
            'captcha': ''
        }
        try:
            r = requests_retry_session().post(url, headers=headers, data=payload)
            data = r.json()
            content = parse_data(data)
            cache.set(key, json.dumps(content, ensure_ascii=False), ex=30600)
        except Exception as e:
            logging.exception(e)
            return None

    return NdsPayerContent(**content)


if __name__ == "__main__":
    res = start(inn='201170210')
    print(res)
