import os

from dotenv import load_dotenv

load_dotenv()


class Settings:
    PROJECT_NAME: str = os.getenv("PROJECT_NAME", 'NDS payer UZ')
    ROOT_PATH: str = os.getenv("ROOT_PATH", '')
    SOURCE_IP: str = os.getenv("SOURCE_IP")
    NDS_MAIN_URL: str = os.getenv('NDS_MAIN_URL')
    NDS_ORIGIN: str = os.getenv('NDS_ORIGIN')
    NDS_REFERER: str = os.getenv('NDS_REFERER')

    REDIS_HOST = os.getenv("REDIS_HOST")
    REDIS_PORT = os.getenv("REDIS_PORT")
    REDIS_DB = os.getenv("REDIS_DB")


os.makedirs('logs', exist_ok=True)

settings = Settings()
