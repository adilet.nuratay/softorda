import requests
from requests.adapters import HTTPAdapter
from requests_toolbelt.adapters import source
from urllib3.exceptions import InsecureRequestWarning
from urllib3.util import Retry
import urllib3

from config import settings

urllib3.disable_warnings(InsecureRequestWarning)


def requests_retry_session(retries=5, backoff_factor=1, status_forcelist=(500, 502, 503, 504), session=None):
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        method_whitelist=False,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist
    )
    if settings.SOURCE_IP:
        adapter = source.SourceAddressAdapter(settings.SOURCE_IP, max_retries=retry)
    else:
        adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session
